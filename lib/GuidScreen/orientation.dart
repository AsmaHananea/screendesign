import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:screenflutter/GuidScreen/Screens/homelandescape.dart';
import 'package:screenflutter/GuidScreen/Screens/homeportrait.dart';



class Orientationn extends StatelessWidget {
  const Orientationn({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        
          child: OrientationBuilder(
            builder: (context, orientation) =>
                orientation == Orientation.portrait
                    ? HomePortrait()
                    : HomeLandescape(),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.rotate_left),
          onPressed: () {
            final isPortrait =
                MediaQuery.of(context).orientation == Orientation.portrait;

            if (isPortrait) {
              setLandscape();
            } else {
              setPortrait();
            }
          },
        ),
      );

  
      
      // body: OrientationBuilder(
      //   builder: (context, orientation) {
      //     if (orientation == Orientation.portrait) {
      //       return HomePortrait();
      //     } else {
      //       return HomePortrait();
      //     }
      //   },
      // ),
    

  }
}
 Future setPortrait() async => await SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);

  Future setLandscape() async => await SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);