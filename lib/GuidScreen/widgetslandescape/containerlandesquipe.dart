import 'package:flutter/material.dart';

class ContainerLanscape extends StatelessWidget {
  final double whidth ; 
  final String title1 ; 
  final String title2 ; 
  final Widget icon1 ; 
  final Widget icon2 ; 
   final String title3 ; 
  final Widget icon3 ; 
  final Widget widgett ;

   ContainerLanscape({
     required this.whidth ,
     required this.title1 , 
     required this.title2 ,
     required this.icon1 , 
      required this.title3,
     required this.icon3 , 
     required this.icon2 , 
     required this.widgett ,

     })  ;

  @override
  Widget build(BuildContext context) {
    return Container(
      
      decoration: BoxDecoration(
       color:   Color(0XFF434343),
       borderRadius: BorderRadius.circular(10) , 
      ),
      
      width: whidth,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
         
         

          children: [
            Row(children: [
              Text(title1 , 
              style: TextStyle(
              color: Colors.white , 
              fontWeight: FontWeight.w500 , 
              fontSize: 12 ,
              
              ),
              ),
               Spacer() , 
              icon1 ,
            ],) ,
            Row(children: [
              Text(title2 , 
              style: TextStyle(
              color: Colors.white , 
              fontWeight: FontWeight.w400 , 
              fontSize: 10 ,
              
              ),
              ),
              Spacer() , 
             icon2 , 
            ],) ,
            Row(children: [
              Text(title3 , 
              style: TextStyle(
              color: Colors.white , 
              fontWeight: FontWeight.w400 , 
              fontSize: 10 ,
              
              ),
              ),
              Spacer() , 
              icon3 ,
            ],) ,
            Spacer() , 
             widgett , 
          ],
        ),
      ),
    ) ; 
  }
}
