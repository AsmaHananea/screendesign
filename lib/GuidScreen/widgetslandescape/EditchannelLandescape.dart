import 'package:flutter/material.dart';
class EditChannel extends StatelessWidget {
  const EditChannel({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
                 
                
                 
                  
                height: MediaQuery.of(context).size.height *0.083,
                child: 
                Row(children: [
                  Container(
                    width: MediaQuery.of(context).size.width*0.12,
                    
                    decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                  color: Colors.amber , 
                    
                  
                          ) , 
                          
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 2),
                          child: Column(
                  
                  
                  children: [
                Text('Edit Channels' , 
                  style: TextStyle(color: Colors.white , fontSize: 10,fontWeight : FontWeight.w900),),
                  Icon(
                          Icons.arrow_drop_down, 
                          color: Colors.white,
                          size : 10 ,
                          
                        ), 
                   
                  ],
                          ),
                        ),
                    
                  ),
                  SizedBox(width: 3,) , 
                   Row(
                     
                     children: [
                     Container(
                         width: MediaQuery.of(context).size.width*0.846,
                     
                       decoration: BoxDecoration(
                      gradient: LinearGradient(
                              end: Alignment.topRight,
                              begin: Alignment.bottomLeft,
                              colors: [
                               
                                   Color(0XFF420075),
                               
                              
                   Colors.blueAccent.shade100 , 
                   
                              ],
                      ), 
                                 ),
                       child: Center(
                         child: SingleChildScrollView(
                          
                           
                             scrollDirection: Axis.horizontal,
                           child: Row(
                            
                             children: [
                               Text(' 3:00   . 3:30   . 4:00   . 4:00 . 4:30  . 5:00  . 5:30   .' , 
                               style: TextStyle(
                                 color: Colors.white ,
                                 fontSize: 13, 
                                 fontWeight: FontWeight.w600 , 
                               ),
                               ),
                               Text(' 6:00   . 6:30  . 7:00   . 7:30  . 8:00   . 9:00  .   9:30 .  10:00. 10:00   . 10:30  . 11:00   . 11:30  . 8:00   . 9:00  .   9:30 .  10:00.' , 
                               style: TextStyle(
                                 color: Colors.white , 
                                  fontSize: 13, 
                                 fontWeight: FontWeight.w800 , 
                               ),
                               ),
                               
                             ],
                           ),
                         ),
                       ),
                     ),
                     ],
                   )
                ],),
                  );
  }
}

