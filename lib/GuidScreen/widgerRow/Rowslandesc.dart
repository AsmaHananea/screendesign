import 'package:flutter/material.dart';
import 'package:screenflutter/GuidScreen/widgetslandescape/ImageInside.dart';

class RowsLandescape extends StatefulWidget {
  final Widget widgeett ;
  final AssetImage imaggge ;
  const RowsLandescape({
    required this.widgeett , 
    required this.imaggge , 

   }) ;

  @override
  _RowsLandescapeState createState() => _RowsLandescapeState();
}

class _RowsLandescapeState extends State<RowsLandescape> {
  @override
  Widget build(BuildContext context) {
    return  Container( 
     
                       width: MediaQuery.of(context).size.width,
                       height: MediaQuery.of(context).size.height*0.23,
                   child: Row(
                   
                     children: [
                       
                        ImageIsidelandecipe(
                          imagee:widget.imaggge , 
                   
                        ) , 
                        SizedBox(width :3 ), 
                       Container(
                         width: MediaQuery.of(context).size.width*0.82,
                         height: MediaQuery.of(context).size.height*0.23,
                         child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,

                           child: widget.widgeett , 
                         ),
                       )
                   
                     ],
                                    
                   ),
                 )  ;
                     
  }
}