import 'package:flutter/material.dart';
import 'package:screenflutter/obee/Screens/Connectingfile.dart';
import 'package:screenflutter/obee/Screens/HomePortrait.dart';
import 'package:screenflutter/obee/Screens/LoginWifi.dart';
import 'package:screenflutter/obee/Screens/SeccefulConnection.dart';

import 'obee/Screens/Connectwif.dart';
import 'obee/Screens/FailedConnection.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       routes: {
    
        HomePage1.id: (context) => const HomePage1(),
  
        HomePortrait.id: (context) =>  HomePortrait(),
        LoginWifi.id: (context) =>  LoginWifi(),
        ConnectingWifi.id: (context) =>  ConnectingWifi(),
         FailedConnection.id: (context) =>  FailedConnection(),
           SEccufulConnection.id: (context) =>  SEccufulConnection(),
  },
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: HomePage1(),
    );
  }
}


