import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:screenflutter/homescreen/Screens/channelBrowse_portrait.dart';

import 'Screens/chnnelBrows_landescape.dart';


class L extends StatefulWidget {
  const L({ Key? key }) : super(key: key);

  @override
  _LState createState() => _LState();
}

class _LState extends State<L> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
       
          child: OrientationBuilder(
            builder: (context, orientation) =>
                orientation == Orientation.portrait
                    ? ChannelBrowse()
                    : ChannelLandescape(),
          ),
        ),
        floatingActionButton: GestureDetector(child: Icon(Icons.rotate_left ,) , 
         onTap: () {
          final isPortrait =
              MediaQuery.of(context).orientation == Orientation.portrait;

          if (isPortrait) {
            setLandscape();
          } else {
            setPortrait();
          }
        },
        ),
      );
       }
}

  Future setPortrait() async => await SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);

  Future setLandscape() async => await SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);

  Future setPortraitAndLandscape() =>
      SystemChrome.setPreferredOrientations(DeviceOrientation.values);

    
 