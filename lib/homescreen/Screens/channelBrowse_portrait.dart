import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';


import '../Widgets/Card.dart';

class ChannelBrowse extends StatefulWidget {
  const ChannelBrowse({Key? key}) : super(key: key);

  @override
  _ChannelBrowseState createState() => _ChannelBrowseState();
}

class _ChannelBrowseState extends State<ChannelBrowse> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leadingWidth: 100,
        leading: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.all_inclusive,
              color: Colors.pink,
              size: 36,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('SMPL'),
                Text(
                  'TV',
                  style: TextStyle(color: Colors.amber),
                ),
              ],
            ),
          ],
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 17),
            child: Icon(Icons.menu, size: 40, color: Colors.amber),
          ),
        ],
      ),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            expandedHeight: MediaQuery.of(context).size.height*0.3 , 
            
            flexibleSpace: FlexibleSpaceBar(background:
             Padding(
               padding: const EdgeInsets.all(5.0),
               child: Stack(
                alignment: Alignment.bottomLeft,
                children: <Widget>[
                  Container(
                    width :MediaQuery.of(context).size.width  , 
                    
                    child: Image(
                      image: AssetImage('images/image1.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                  bottom: 0,
                    child: Container(
                     
                    
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 300,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: <Color>[
                                        Colors.pink.withOpacity(1),
                                        Colors.pink.withOpacity(0.5),
                                        Colors.pink.withOpacity(0.1),
                                      ],
                                    ),
                                  ),
                                  child: Text(
                                    '  CONTINUE WATCHING ?',
                                    style: TextStyle(
                                      color: Colors.grey.shade300,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                                SizedBox(height: 3),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        '  BIG TIMBER',
                                        style: TextStyle(
                                          color: Colors.amber,
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        ' | 2021 | TV-14 | 48m',
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 5),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        '    A Slightly red neck logger works his but off and operates at..',
                                        style: TextStyle(fontSize: 10),
                                      ),
                                      Text(
                                        'MORE',
                                        style: TextStyle(
                                          color: Colors.amber,
                                          fontSize: 10,
                                        ),
                                      ),
                                      SizedBox(width: 5,) ,
                                      Container(
                                        width: 20,
                                        child: Image(image: AssetImage('images/firstcon.png') ))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            children: [],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
            ),
             ),),

          ),
          SliverList(delegate:SliverChildListDelegate([
            Container(
               height: MediaQuery.of(context).size.height*0.82,
             
              
            child: Column(
              children: [
                  Container(
                    child: Row(
            children: <Widget>[
              Expanded(
                child: GestureDetector(
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(10, 12, 4, 5),
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          child: Center(
                            child: Text(
                              'CHANNEL GUIDE',
                              style: TextStyle(
                                color: Colors.amber,
                                fontSize: 20,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        height: 82,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.amber, width: 1),
                        ),
                      ),
                    ),
                ),
              ),
              Expanded(
                child: GestureDetector(
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(4, 12, 10, 5),
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          child: Center(
                            child: Text(
                              'BROWSE RECORDINGS',
                              style: TextStyle(
                                color: Colors.amber,
                                fontSize: 20,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        height: 82,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.amber, width: 1),
                        ),
                      ),
                    ),
                ),
              ),
            ],
            ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                           
                                      
                        children: [
                         Card_rowChannel(
                                  imageFilm: AssetImage('images/image2.jpg'),
                                  titleFilm: 'TNT | Dances with W..',
                                  sousTitleFilm: 'PG-13 | 3h56m | 1990',
                                  descriptionFilm:
                      'A Civil War soldier develops a relationship with a band of Lakota Indians. Attracted by the simplicity of their lifestyle, he chooses to leave his...',
                                ),
                                Card_rowChannel(
                                    imageFilm: AssetImage('images/image3.png'),
                                    titleFilm: 'ABC | Shark Tank',
                                    sousTitleFilm: 'S1:E8 Episode Title',
                                    descriptionFilm:
                        'Description info as we see fit to provide the viewer and or see fit for said channel as it relates to the specific channel.'),
                                Card_rowChannel(
                                    imageFilm: AssetImage('images/image4.png'),
                                    titleFilm: 'BBC | Super Me',
                                    sousTitleFilm: 'TV-14 | 103m | 2019',
                                    descriptionFilm:
                        'A struggling screenwriter discovers his lucrative ability to bring antiques from his dreams into the real world -- but his new life soon unravels. '),
                                Card_rowChannel(
                                  imageFilm: AssetImage('images/imagevide.png'),
                                  titleFilm: '',
                                  sousTitleFilm: '',
                                  descriptionFilm: ' ',
                                ),
                                Card_rowChannel(
                                  imageFilm: AssetImage('images/imagevide.png'),
                                  titleFilm: '',
                                  sousTitleFilm: '',
                                  descriptionFilm: ' ',
                                ),
                                Card_rowChannel(
                                  imageFilm: AssetImage('images/imagevide.png'),
                                  titleFilm: '',
                                  sousTitleFilm: '',
                                  descriptionFilm: ' ',
                                ),
                                      
                      ],),
                    ),
                  )
              ],
              
            ),
          )

          ]) ),
        ],
      ),);}}

      