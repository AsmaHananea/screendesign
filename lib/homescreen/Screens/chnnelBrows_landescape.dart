import 'package:flutter/material.dart';
import 'package:screenflutter/homescreen/Widgets/Containner.dart';


class ChannelLandescape extends StatefulWidget {
  

  @override
  _ChannelLandescapeState createState() => _ChannelLandescapeState();
}

class _ChannelLandescapeState extends State<ChannelLandescape> {
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
     body : Column(
       children: [
         Expanded(
           flex: 2,
           child: Stack(
             children: [
               Container(
                 width: MediaQuery.of(context).size.width , 
                  height: MediaQuery.of(context).size.height*0.6,
                 child: Padding(
                   padding: const EdgeInsets.symmetric(horizontal: 130 ) , 
                   child: Container(
                     color: Colors.white,
                    
                     width: MediaQuery.of(context).size.width*0.6,
                     child:Stack(
                        alignment: Alignment.bottomLeft,
                        children: <Widget>[
                        Container(
                             height: MediaQuery.of(context).size.height*0.6,
                     width: MediaQuery.of(context).size.width,
                            
                            child: Image(
                              image: AssetImage('images/image1.png' , 
                              
                              
                              ),
                              fit: BoxFit.fill,
                            ),
                          ),
                        Positioned(
                          bottom: 0,
                            child: Container(
                             
                            
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 5),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          width: 300,
                                          decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                              begin: Alignment.centerLeft,
                                              end: Alignment.centerRight,
                                              colors: <Color>[
                                                Colors.pink.withOpacity(1),
                                                Colors.pink.withOpacity(0.5),
                                                Colors.pink.withOpacity(0.1),
                                              ],
                                            ),
                                          ),
                                          child: Text(
                                            '  CONTINUE WATCHING ?',
                                            style: TextStyle(
                                              color: Colors.grey.shade300,
                                              fontSize: 14,
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 3),
                                        Container(
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                '  BIG TIMBER',
                                                style: TextStyle(
                                                  color: Colors.amber,
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              Text(
                                                ' | 2021 | TV-14 | 48m',
                                                style: TextStyle(
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(height: 5),
                                        Container(
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                '    A Slightly red neck logger works his but off and operates at..',
                                                style: TextStyle(fontSize: 10),
                                              ),
                                              Text(
                                                'MORE',
                                                style: TextStyle(
                                                  color: Colors.amber,
                                                  fontSize: 10,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    children: [],
                                  ),
                                ],
                              ),
                            ),
                          ),
                         Positioned(
                           top: 10,
                           right: 10,
                           child: GestureDetector(
                             child: Icon(Icons.brightness_5_outlined , 
                             color: Colors.amber,
                             ),
                           )),
                          Positioned(
                            bottom: 10,
                           right: 10,
                 
                            child:
                            Container(
                              height: 40 , 
                              width: 70,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey.shade100,
                                  width: 2 , 
                                ), 
                                borderRadius: BorderRadius.circular(10) , 
                                
                 
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(top: 7),
                                child: Column(
                                  children: [
                                    Row(
                                      
                                      children: [
                                        Icon(Icons.keyboard_arrow_left , size: 15,), 
                                        Container(
                                           height: 15 , 
                                    width: 30,
                                          color: Colors.grey.shade400,
                                        ) ,
                                          Icon(Icons.keyboard_arrow_right , size: 15,), 
                                      ],
                                    ),
                                       Icon(Icons.expand_more , size: 13,), 
                                  ],
                                ),
                              ),
                            )
                             ),
                         
                        ],
                    ),
                   ),
                 ),
               ),
               Positioned(
                 top: 15,
                 right : 30 ,
                 child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Icon(
              Icons.all_inclusive,
              color: Colors.pink,
              size: 36,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('SMPL'),
                Text(
                  'TV',
                  style: TextStyle(color: Colors.amber),
                ),
              ],
            ),
            ],
               ), )
             ],
           ),
         ), 
         
         Expanded(

           
           child: Center(
             child: Container(
               height: MediaQuery.of(context).size.height*0.5,
               width: MediaQuery.of(context).size.width*0.8,
                
         
               child: Row(

                  mainAxisAlignment : MainAxisAlignment.spaceBetween , 
                 
                 children: [
                
                Contaiiner(
                  size : 10 , 
                  imageFilm: Padding(
                 padding: const EdgeInsets.symmetric(horizontal: 20 , vertical: 20),
                  child: Center(
                    child: Container(
                       height: MediaQuery.of(context).size.height*0.13,
                      child: Column(
                                  children: [
                                    Text(
                                      'CHANNEL ',
                                      style: TextStyle(
                                        color: Colors.amber,
                                        fontSize: MediaQuery.of(context).size.width*0.02,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      ' GUIDE',
                                      style: TextStyle(
                                        color: Colors.amber,
                                        fontSize: MediaQuery.of(context).size.width*0.02,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                    ),
                  ),
                ), 
                  color: Colors.amber ,
                          ) , 
        
                        
                        
                        
                  Contaiiner(
                            color: Colors.redAccent , 
                            size: 4 ,
                          imageFilm: Image( 
                            image :  AssetImage(
                            'images/image2.jpg') ,
                            fit: BoxFit.cover,
                            ),
                            ), 
                          
                        
                        
                        
                  Contaiiner(
                            color: Colors.redAccent , 
                            size: 4,
                          imageFilm: Image( 
                            image :  AssetImage(
                            'images/image4.png') ,
                            fit: BoxFit.cover,
                            ),), 
                        
                        
                        
                 Contaiiner(
                  size : 10 , 
                  imageFilm: Center(
                    child: Container(
                      height: MediaQuery.of(context).size.height*0.145,
                      child: Column(
                                  children: [
                                    Text(
                                      'BROWS ',
                                      style: TextStyle(
                                        color: Colors.amber,
                                        fontSize: MediaQuery.of(context).size.width*0.02,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      ' RECORDING',
                                      style: TextStyle(
                                        color: Colors.amber,
                                        fontSize: MediaQuery.of(context).size.width*0.02,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                    ),
                  ), 
                  color: Colors.amber ,
                          ) , 


                 
         
               ],),
             ),
           ),
         )
       ],
     )

    ); 
  }
}