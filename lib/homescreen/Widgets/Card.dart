import 'package:flutter/material.dart';

class Card_rowChannel extends StatelessWidget {
  Card_rowChannel(
      {required this.imageFilm,
      required this.titleFilm,
      required this.sousTitleFilm,
      required this.descriptionFilm});
  final AssetImage imageFilm;
  final String titleFilm;
  final String sousTitleFilm;
  final String descriptionFilm;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: GestureDetector(
        onTap: () {},
        child: Container(
          height: MediaQuery.of(context).size.height*0.22,
          decoration: BoxDecoration(
            color: Colors.grey[800],
            border: Border.all(
              color: Color(0xFF757575),
              width: 1.0,
            ),
            borderRadius: BorderRadius.circular(2),
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 191,
                child: Container(
                  child: Image(
                    image: imageFilm,
                   
                  ),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey.shade400,
                      width: 1.0,
                    ),
                    borderRadius: BorderRadius.circular(1),
                  ),
                ),
              ),
              Expanded(
                flex: 207,
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 6,
                      ),
                      Text(
                        titleFilm,
                        style: TextStyle(
                          fontSize: MediaQuery.of(context).size.height*0.015,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        sousTitleFilm,
                        style: TextStyle(
                          fontSize: MediaQuery.of(context).size.height*0.013,
                          color: Colors.pink,
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height*0.019,
                      ),
                      Text(
                        descriptionFilm,
                        style: TextStyle(
                          fontSize: MediaQuery.of(context).size.height*0.013,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
