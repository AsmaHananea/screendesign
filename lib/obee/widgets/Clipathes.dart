import 'package:flutter/material.dart';

class CustomClipPath extends CustomClipper<Path> {
  var radius=10.0;
  @override
   Path getClip(Size size) {
    Path path = Path();
    path.addPolygon([
      Offset(0, size.height),
      Offset(size.width / 2, 0),
      Offset(size.width, size.height)
    ], true);
    return path;
  }

  @override
   bool shouldReclip(CustomClipper<Path> oldClipper) => false;
  }
class CustomClipPathe1 extends CustomClipper<Path> {
  var radius=10.0;
  @override
   Path getClip(Size size) {
    Path path = Path();
    path.lineTo(size.width / 2, size.height);
    path.lineTo(size.width, 0.0);
    return path;
  }

  @override
   bool shouldReclip(CustomClipper<Path> oldClipper) => false;
  }
  