import 'package:flutter/material.dart';

class Wifi extends StatefulWidget {
  late String title;
  final Widget image;

  Wifi(
      {
      required this.title,
       required this.image});

  
  
  @override
  _WifiState createState() => _WifiState();
}

class _WifiState extends State<Wifi> {
  
  
  
  bool enabled = false;

  @override
  Widget build(BuildContext context) {
      return Row(
          children: [
            GestureDetector(
            child:  Text(
                    widget.title ,
                    style: TextStyle(
                      fontSize: MediaQuery.of(context).size.height*0.0199,
                      color: (enabled
                        ? Color(0XFFff1e56) 
                        : Colors.white
                      ),
                      fontWeight: FontWeight.w600
                    ),
                  
              
             
            ),
            onTapDown: (TapDownDetails details){
              setState(() {
                enabled = true;
              });
            },
            onTapUp:( TapUpDetails details){
              setState(() {
                enabled = false;
              });
            }, 
               
            
            
              ),
              Spacer() , 
           Container(
             height: MediaQuery.of(context).size.height*0.021,
             width: 24,
             child: widget.image) , 
          ],
        
      );
  }
}


